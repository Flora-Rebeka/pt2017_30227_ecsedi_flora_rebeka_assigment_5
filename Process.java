import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class Process {
	ArrayList<MonitoredData> data;
	
	ArrayList<MonitoredData> readFromFile(){
		data = new ArrayList<MonitoredData>();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
        String fileName = "file.txt";
        String line = null;

        try {
            FileReader fileReader = new FileReader(fileName);
            
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            String firstLine = bufferedReader.readLine();
            String nextLine = bufferedReader.readLine();
            System.out.println(firstLine);
            System.out.println(nextLine);
            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                
                String s = line;
                String ss = "";
                String sss = "";
                int i = 0;
                while(i <= 18){
                	ss = ss + Character.toString(s.charAt(i));
                	i++;
                }
                i += 13;
                while(i <= 50){
                	sss = sss + Character.toString(s.charAt(i));
                	i++;
                }
                String ssss = line;
                ssss = ssss.replaceAll("[0-9]","");
                ssss = ssss.replaceAll(":","");
                ssss = ssss.replaceAll(" ","");
                ssss = ssss.replaceAll("-","");
                ssss = ssss.replaceAll("	","");
                
                LocalDateTime parsed = LocalDateTime.parse(ss, formatter);
                LocalDateTime parsed2 = LocalDateTime.parse(sss, formatter);
                LocalDateTime start = parsed;
                LocalDateTime end = parsed2;
                String activity = ssss;
                
                data.add(new MonitoredData(start,end,activity));
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");
        }
        return data;
	}
	
	int countDistinctDays(ArrayList<MonitoredData> data){
		return ((ArrayList<Integer>) data.stream().map(MonitoredData::getDayOfMonth).sorted(( o1,  o2)->o2.compareTo(o1)).distinct().collect(Collectors.toList())).size();
	}
	
	void mapDistinctActions(){
		Map <String, Long> map = data.stream().collect(Collectors.groupingBy((MonitoredData d) -> d.getActivity(), Collectors.counting()));
		
		PrintWriter out = null;
		for (Map.Entry m : map.entrySet()) {
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter("fisier.txt", true)));
				out.println(m.getValue() + " : " + m.getKey()); 
				System.out.println("Success!");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			out.close();
		}
	}
	
	void dataStructure(){
		Map<Integer, Map<String, Long>> dataStructure = data.stream().collect(Collectors.groupingBy((MonitoredData d1) -> d1.getDayOfMonth() , Collectors.groupingBy((MonitoredData d2) -> d2.getActivity(), Collectors.counting())));
		PrintWriter out = null;
		for (Map.Entry m : dataStructure.entrySet()) {
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter("fisier2.txt", true)));
				out.println(m.getValue() + " : " + m.getKey()); 
				System.out.println("Success!");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			out.close();
		}
	}
}

import java.util.ArrayList;

public class MainClass {
	
	public static void main(String []args){
		Process p = new Process();
		ArrayList<MonitoredData> data = p.readFromFile();
		for(MonitoredData d: data){
			System.out.println(d.getStartTime() + "    ");
		}
		for(MonitoredData d: data){
			System.out.println(d.getEndTime() + "    ");
		}
		for(MonitoredData d: data){
			System.out.println(d.getActivity());
		}
		System.out.println(p.countDistinctDays(data));
		p.mapDistinctActions();
		p.dataStructure();
	}
}
